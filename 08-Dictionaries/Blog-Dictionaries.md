
## Python Dictionaries:

In this python for beginners tutorial, we will learn about dictionaries and iterating over them. Learning dictionaries will help you understand the fundamentals of storing data to use later. Dictionaries don't allow you to have duplicate keys, but the value of keys can be the same.

## What is a Python Dictionary?
 A dictionary consists of a collection of key-value pairs. Each key-value pair maps the key to its associated value. Each key has to be unique or their value will just be overwritten.

Dictionaries are used to take data and store it in keys that have values. When creating a dictionary we use curly brackets to begin then between the key and the value we use a colon and separate each pair by commas. The keys and values can be strings, integers, floats, tuples, and more. Dictionaries don't allow you to have duplicate keys, but the value of keys can be the same.

## Course Objectives:  

  -  Creating a Dictionary 
  -  Types of Keys and Values
  -  Leverage Dictionaries
  -  Dictionary Cheat Sheet

## Creating a Dictionary 
In the language of a Python dictionary, the word would be the key and the definition would be the value. Let's check out an example.

### Creating an empty dictionary


```python
example = {}
```

### Creating your first dictionary with data


```python
example = {'word': 'definition'}
print(example)
```

    {'word': 'definition'}


### Using the type function


```python
print(type(example))
```

    <class 'dict'>


## Types of Keys and Values

### Strings


```python
user = {'first_name': 'Jack', 'last_name': 'Benimble'}
print(user)
```

    {'first_name': 'Jack', 'last_name': 'Benimble'}


### Integers and Floats


```python
int_floats = {1: 1, 2: 2.5, 3: 3.75, 2.50: 5}
print(int_floats)
```

    {1: 1, 2: 2.5, 3: 3.75, 2.5: 5}


### Tuples


```python
lunch = {('main1', 'main2'): ('hotdog', 'bun'), ('side1', 'side2'): ('fries', 'drink')}
print(lunch)
```

    {('main1', 'main2'): ('hotdog', 'bun'), ('side1', 'side2'): ('fries', 'drink')}


### List
List can not be keys only values


```python
book = {'chapters': ([1, 2]), 'sections': ([1.1, 1.2, 2.1, 2.2])}
print(book)
```

    {'chapters': [1, 2], 'sections': [1.1, 1.2, 2.1, 2.2]}


## Leverage dictionaries

### Storing, Adding, Updating, Deleting Data


```python
test_dict = {}
print(type(test_dict))

test_dict = {'key1': 'value1'}
print(test_dict)

test_dict['key2'] = 'value2'

test_dict2 = {'key3': 'value3'}
test_dict.update(test_dict2)
print(test_dict)

test_dict.pop('key3')
print(test_dict)
```

    <class 'dict'>
    {'key1': 'value1'}
    {'key1': 'value1', 'key2': 'value2', 'key3': 'value3'}
    {'key1': 'value1', 'key2': 'value2'}


### List to Dictionaries


```python
first_names = ['Jack', 'Mary', 'John']
last_names = ['Benimble', 'Jane', 'Doe']
users = dict(zip(first_names, last_names)) 
print(users)
```

    {'Jack': 'Benimble', 'Mary': 'Jane', 'John': 'Doe'}


### Iterating Over Dictionaries With Lists For Values


```python
book = {'chapters': ([1, 2]),
        'sections': ([1.1, 1.2, 2.1, 2.2]),
        'sub_sections': (['1.1a', '1.1b', '1.2a', '1.2b', '2.1a', '2.1b', '2.2a', '2.2b'])}
for key, values in book.items():
  print(key)
  if isinstance(values, list):
    for value in values:
      print(value)
  else:
      print(value)
```

    chapters
    1
    2
    sections
    1.1
    1.2
    2.1
    2.2
    sub_sections
    1.1a
    1.1b
    1.2a
    1.2b
    2.1a
    2.1b
    2.2a
    2.2b


### Iterating Over Dictionaries


```python
for key, value in users.items():
  print(key,value)
```

    Jack Benimble
    Mary Jane
    John Doe


### Nesting Dictionaries


```python
nested = {'dictA': {'key1': 'value1',
                    'key2': 'value2'},
          'dictB': {'key3': 'value3',
                    'key4': 'value4'}
          }
nested['dictC'] = ({'key5': 'value5', 'key6': 'value6'})
print(nested)
```

    {'dictA': {'key1': 'value1', 'key2': 'value2'}, 'dictB': {'key3': 'value3', 'key4': 'value4'}, 'dictC': {'key5': 'value5', 'key6': 'value6'}}


### Iterating Over Nested Dictionaries


```python
def nested_dict_iterator(nested_dict):
  for key, value in nested_dict.items():
    if isinstance(value, dict):
      for pair in nested_dict_iterator(value):
        yield(key, *pair)
      else:
        yield(key, value)
for pair in nested_dict_iterator(nested):
    print(pair)
```

    ('dictA', {'key1': 'value1', 'key2': 'value2'})
    ('dictB', {'key3': 'value3', 'key4': 'value4'})
    ('dictC', {'key5': 'value5', 'key6': 'value6'})


## Dictionary Methods Cheat Sheet

### Definition

x = {key1:value1, key2:value2} 

### Operations

- len(dictionary) - Returns the number of items in the dictionary

- for key in dictionary - Iterates over each key in the dictionary

- for key, value in dictionary.items() - Iterates over each key,value pair in the dictionary

- if key in dictionary - Checks whether the key is in the dictionary

- dictionary[key] - Accesses the item with key key of the dictionary

- dictionary[key] = value - Sets the value associated with key

- del dictionary[key] - Removes the item with key key from the dictionary

### Methods

- dict.get(key, default) - Returns the element corresponding to key, or default if it's not present

- dict.keys() - Returns a sequence containing the keys in the dictionary

- dict.values() - Returns a sequence containing the values in the dictionary

- dict.update(other_dictionary) - Updates the dictionary with the items coming from the other dictionary. Existing entries will be replaced; new entries will be added.

- dict.clear() - Removes all the items of the dictionary

Check out the official documentation for [dictionary operations and methods](https://docs.python.org/3/library/stdtypes.html#mapping-types-dict).  
