# Learn Python Data Types and Variables

In this python for beginners tutorial, we will learn about data types and variables. Learning data types and variables will help you understand the fundamentals to start building something fun. So what are data types and variables? 

A variable is nothing more than a container for a stored value in memory. You reserve space in memory using an assigned name, and later you can use it to refer to it in the program. Variables can store data of differing types, and each data type can do different things. Store a whole number, store a set of characters, or store an entire set of strings. Another reason to love python is that you do not need to declare the data type before assigning it a value explicitly. Based on the value set, the interpreter decides its data type. You can always store a different type in a variable. Python can determine what data type by the value given. 

So, let’s start your learning journey into python!

## Learning Objectives
- Variables and Types
  - Integers
  - Floats
  - Strings
    - Formatting
    - Quotes
    - Concatenation
  - Multiple Assignments
  - Conversions
  - Other Data types

### Integers
- Whole numbers 
- Can be both positive and negative
- Length of number depends on the memory of your computer


```python
thisIsAnInt = 5
print(type(thisIsAnInt))
print(thisIsAnInt)
```

    <class 'int'>
    5



```python
thisIsAlsoAnInt = -5
print(type(thisIsAlsoAnInt))
print(thisIsAlsoAnInt)
```

    <class 'int'>
    -5



```python
sumOf = thisIsAnInt + thisIsAlsoAnInt
print(type(sumOf))
print(sumOf)
```

    <class 'int'>
    0


### Floats
- Not an integer, but a fraction of an integer
- Represented as a decimal
- Division will return a float


```python
thisIsAFloat = 0.05
print(type(thisIsAFloat))
print(thisIsAFloat)
```

    <class 'float'>
    0.05



```python
divOf = 10 / 5
print(type(divOf))
print(divOf)
```

    <class 'float'>
    2.0


### Strings
- Used to represent text
- Implemented as an list structure


```python
thisIsAString = "This is a String"
print(type(thisIsAString))
print(thisIsAString)
```

    <class 'str'>
    This is a String



```python
thisIsAStringToo = 'This is a String Too'
print(type(thisIsAStringToo))
print(thisIsAStringToo)
```

    <class 'str'>
    This is a String Too



```python
thisIsAString[0]
```




    'T'



#### Formatting
- There are several ways to format
  - Older method using the '%'
  - Preferred method to format a string 'str.format'
  - The new method using interpolation (f-string)



```python
myString = "world"

oldMethod = "Hello %s" % myString
strFormat = "Hello {}".format(myString)
interPol = f'Hello {myString}'

print(oldMethod)
print(strFormat)
print(interPol)
```

    Hello world
    Hello world
    Hello world


#### Quotes
- What qoutes you choose will matter
- Using one set of quotes will allow the other
  - IE: Double qoutes allow the use of an apostrophe
- You can also escape characters



```python
# Fails Intentionally
qoutesMatter = 'Don't let the qoutes fool you''
print(qoutesMatter)
```


      File "<ipython-input-51-8604f122123c>", line 2
        qoutesMatter = 'Don't let the qoutes fool you''
                            ^
    SyntaxError: invalid syntax




```python
usingAnEscChar = 'You can\'t use single quotes here unless you escape'
print(usingAnEscChar)
```

#### Multi-line String
- Using triple single or double qoutes will allow both single and double apostrophies
- Keeps carriage return/new lines
- Allows many other special characters



```python
multiLineString = '''
This is a multiline string
Notice how it can handle both '' and "" 
It can also handle special characters !@#$%^&*() 

Also keeps carriage returns/new lines
'''
print(multiLineString)
```

### Multiple Assignments
- Python allows multiple assignments


```python
a, b, c = 1, 2.0, "String"
print(a)
print(b)
print(c)
```

### Conversions
- At times you may recieve a number as a string
- A string will cause a math operation to fail
- A visual clue that it is a string and not a number are the appearance of qoutes



```python
a = '1234'
a = int(a) # Convert the string to an integer
b = 5
print('a is of type: {}'.format(type(a)))
print('b is of type: {}'.format(type(b)))
a + b
```

### Other Data types
- There are a ton of data types, here are just a few
- Will talk more on these later
  - Lists
  - Dictionaries
  - Tuple 


```python
myList = [1,2,3,4]
myDict = {'foo':'bar'}
myTuple = (1,2,3)
print(myList)
print(myDict)
print(myTuple)
```
