

## String Formatting Operations

Strings are the standard way for Python to store characters as data used in variables. This string data type, also known as `str`, can be formatted in endless ways from capitalizing letters to turning integers into a string. If you need a refresher on string data types and variables, you can head over to our post [Variables and Data Types](https://www.journeyintopython.com/python-data-types-and-variables/). In this course we will go over string formatting and the operators that can be used with strings. We will first start with different ways to format and work our way into using operators to compare strings. What makes string formatting amazing is you can take user input and format it to your liking, in your script. Let's say you have a script that takes a user's input that you need to store in a variable. Though you don't won't user names to be stored with any capital letters. You can then use the operators to compare if the user put in numbers in their username and raise an error for the user. With all that being said, let's begin your Journey Into Python.

## What is String Formatting and Operators?

String formatting is the use of methods to change the structure of that specific string. This can also be applied to turning non-strings like numbers into strings. Operators are a symbol that usually represent an action or process. These symbols were adapted from mathematics and logic.

## Course Objectives:

1. Format Strings
2. String Methods
3. More on Formatting
4. Operators
6. Cheat Sheet


## Format Strings

Formatting strings has a wide range of options, but here we will cover the most commonly used ways of formatting. Here we will learn how to add strings and strings with other data types. First, we will go over the different ways to change strings. Concatenation is the simplest way of add one string to another.


```python
firstName = "John"
lastName = "Schmidt"
print(firstName + " Jacob Jingleheimer " + lastName)
```

    John Jacob Jingleheimer Schmidt


The next way you can format is use `.format()`, this uses index numbers to place them where you need them. You can also use place holder names but that can be unnecessary and can get lengthy.


```python
print("Welcome to {}" .format("'Jouney Into Python'"))

print("Welcome to {}, your go-to {}".format("'Jouney Into Python'","learning blog!!!"))

print("This is the order {}, {}, {}, ect...".format(1,2,3))
```

    Welcome to 'Jouney Into Python'
    Welcome to 'Jouney Into Python', your go-to learning blog!!!
    This is the order 1, 2, 3, ect...



```python
print("{1} {3} {2} {0}.".format("indexing","This","using","is"))
print("{one} {two} {three} {four}.".format(four="naming",one="This",three="using",two="is"))
```

    This is using indexing.
    This is using naming.



## String Methods

You'll notice that we used the `.format()` to change or manipule a string. This is one of Python's methods for strings. Now we will move on to the power of other methods for strings. I will go over a few but if you want to learn about other methods for strings, check out [Python's String Methods](https://docs.python.org/3/library/stdtypes.html#string-methods). The first method to go over is the `.upper()` and `.lower()` to change text size. This also does not change the variable just creates a new instance with the change.


```python
userName = "Hello World!"
print(userName.upper())
print(userName.lower())
print(userName)
```

    HELLO WORLD!
    hello world!
    Hello World!


Another common use method is `.strip()`, this will remove any whitespace around the string. This comes in handy with user input and they put a space before or after the string.


```python
stripString = "     Hello World!     "
print(f"{stripString} is {len(stripString)} characters long.")
print(f"{stripString.strip()} is now only {len(stripString.strip())} characters long.")
```

         Hello World!      is 22 characters long.
    Hello World! is now only 12 characters long.


The `.replace()` takes two arguments, what you want replaced and the replace with. This works great for changing over a companies email address.


```python
email = "john@example.com"
print(email.replace("@example.com","@newemail.org"))
```

    john@newemail.org


Last but not all, the `.split()` method that will take a string of words with a pre-defined separator and turn it into a list. Remember that this does not change the variable, to do that you will have to make it a new variable.


```python
names = "Tom, Jerry, Bugs Bunny, Road Runner, Daffy, Elmer"

print(names, type(names))
print(names.split(","))

splitNames = names.split(",")

print(type(splitNames), splitNames)
```

    Tom, Jerry, Bugs Bunny, Road Runner, Daffy, Elmer <class 'str'>
    ['Tom', ' Jerry', ' Bugs Bunny', ' Road Runner', ' Daffy', ' Elmer']
    <class 'list'> ['Tom', ' Jerry', ' Bugs Bunny', ' Road Runner', ' Daffy', ' Elmer']



## More on Formatting

A non-string type can not be added to a string, like an intager. But you can either change it into a string or use the `.format` that will do it for you. Now let's say you get user input and you don't know what they will enter, but you want that input displayed in your text.


```python
userInput = 99
printOutput = "I am " + userInput + "% sure, I am learning to code."
print(printOutput)
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-123-4761cd18086f> in <module>()
          1 userInput = 99
    ----> 2 printOutput = "I am " + userInput + "% sure, I am learning to code."
          3 print(printOutput)


    TypeError: can only concatenate str (not "int") to str



```python
userInput = 99
printOutput = "I am "+ str(userInput) +"% sure, I am learning to code."
print(printOutput.format(userInput))

userInput = 99
printOutput = "I am {}% sure, I am learning to code."
print(printOutput.format(userInput))
```

    I am 99% sure, I am learning to code.
    I am 99% sure, I am learning to code.


A new format that was implemented in Python 3.6 is the f-string format. This can make simple work out of inputing variables in strings. Also makes it very easy to read by anybody.


```python
name = "Python"
age = 30
print(f"Hello {name}, you are {age} years old.")
```

    Hello Python, you are 30 years old.


Here is the example from the intro, where we take input and make it all lowercase. It is then passed through some if statements to check length and if there are any numbers. Now this isn't logical but you get the idea of what can be done with strings and how you can put that new knowledge to use.


```python
myInput = input("Enter Password: ")

myInput = myInput.lower()

if len(myInput) < 8:
  print(f"'{myInput}' needs to be more than 8 characters long.")
elif myInput.isalpha() == False:
  print(f"'{myInput}' can not have numbers in it.")
else:
  print(f"Your '{myInput}' has been accepted!")
```

    Enter Password: Password
    Your 'password' has been accepted!


You can also format variables using Pythons format operations. Link to [Common String Operations](https://docs.python.org/3/library/string.html#format-examples) in Python.


```python
print("{:*^30}".format("centered"))
print("{:<30}".format("left aligned"))
print("{:>30}".format("right aligned"))
```

    ***********centered***********
    left aligned                  
                     right aligned



## Operators

Operators are mathematcal symbols and logic used to evaluate elements in python. These are things like +, -, =, and, or, not. With these operators you are able to create decisions within Python. Simple use would be is 1 < 2 return true or false. Then you can have Python execute more code depended apon the return value. Here is a list of Python's data type [Operators](https://docs.python.org/3/library/string.html#format-specification-mini-language). The first example shows the value of each letter then compares the alphabet value in the two strings.


```python
stringValue1 = [ ord(x) for x in "Penny" ]
print('Penny:', stringValue1)

stingValue2 = [ ord(x) for x in "Paul" ]
print('Paul:', stingValue2)

if "Penny" > "Paul":
  print("Penny has larger alphabet value.")
elif "Penny" < "Paul":
  print("Paul has smaller alphabet value.")
```

    Penny: [80, 101, 110, 110, 121]
    Paul: [80, 97, 117, 108]
    Penny has larger alphabet value.


Here is example from another course [List of Lists.](https://www.journeyintopython.com/python-lists/) Here = is used for the count, < to compare count to length, += to add one to count, and *in* is used with the for statement. For a list of comparision operators check out Python Documents at this [link](https://docs.python.org/3/reference/expressions.html#comparisons)


```python
nestedList = [["list0index0","list0index1"],["list1index0","list1index1"]]

count = 0
while count < len(nestedList):
  for i in nestedList:
    print(f"Index number is {count}")
    print(i)
    count += 1
```

    Index number is 0
    ['list0index0', 'list0index1']
    Index number is 1
    ['list1index0', 'list1index1']



## Cheat Sheet

Printer-friendly link to [Cheat Sheet](https://www.journeyintopython.com "Thanks For reading to the end or just looking for this link")

### String Formatting:

String formatting is the use of methods to change the structure of that specific string. This can also be applied to turning non-strings like numbers into strings.

### Syntax:

`x = "string's"` or `x = '"string"'`

### Common Mistakes:

Adding intergers to strings without coverting them to str format first.

String methods do not change the original string, only show the change. To implement the change you need to make newstring = string.method().

Triple check your syntax when adding strings with other strings. Don't forget strings need to be in quotes, double quotes if you need to use apostraphes, and single quotes if you use double.

### Operators and Methods for String's:

- `:0<` Shifts "string" to the left dependent on number
- `:0>` Shifts "string" to the right dependent on number
- `:0^` Makes your "string" centered dependent on number
- `and` Returns True if both statements are true	"string" == "string" and "string" == "string"
- `or` Returns True if one of the statements is true	"string" == "string" or "string" == "string"
- `not` Reverse the result, returns False if the result is true	not("string" == "string" and "string" = "string")
- `is` Returns True if both variables are the same object	"string" is "string"
- `is not` Returns True if both variables are not the same object	"string" is not "string"
- `in` Returns True if a sequence with the specified value is present in the object	"string" in "string"
- `not in`	Returns True if a sequence with the specified value is not present in the object	"string" not in "string"
- `.upper() .lower()` Changes size of text in string
- `.strip()` Remove empty space around string
- `.split('seperator')` Change long string with same seperators into a list
- `.replace('what','to')` Replace string with another string, can be one character or entire string
