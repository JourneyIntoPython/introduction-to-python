
# Learn Python Functions


## Python Functions

Let's get that brain going and learn more Python. You ever create a little code here and a little code there, then think, "wish I could reuse that code." Well you're in the right place, because functions do exatly that. Python is like an office building and functions are like all those works in a cubicles. They are all by themselfs just waiting to be called so they can all start working together when they are needed. So let's start your journey into python and learn how to create those workers, so you can use them when you need them.
If you are just now popping into this blog go check out [How to Use Google Colab for Python](https://www.journeyintopython.com/google-colab-python/) and use our code examples to experiment with.


## What are Fuctions?

A function is a block of code which will only work when you call it somewhere in your code. Fucntions can be used multiple times within the same script. Funtcions can take values, known as parameters, that can be used inside that block of code. Where you can return elements to use else where in your script.


## Course Objectives

- Learn how to create a function
- Call the function you created
- Add parameters to your fuction
- What is Return and how to use
- Learn more function parameters 
- Show you a little on recursion
- Cheat sheet for functions


## How To Create A Function

I keep using the word function and it makes me think of that catchy song. Function, Junction, what's your Conjuction by [Schoolhouse Rock](https://youtu.be/0wewGfsrfGY). Back to Python, to create a function you first have to define that your are making a block of code a function. Well if you guesses you use the keyword `def`, then you would be correct. Then you will give your function a name, best to use what the function does, with no spaces. Now at this time you will follow that with parenthises and a colon. The parenthises is where your paramenters will go, We'll give more details later. So let's give that a try.


```python
def questionString():
  print("Why won't this print?")
  print("Because I don't work till called")
```

You will notice after you have created your function, if your following along in, is that functions can also be folded in most IDE's. Also if you tried to run the the code you will notice it didn't print anything. That's because we created the function, now we have to call the function for it to run the code inside.


## How To Call A Function

Well we were thinking we should draw this out and make it elaborate, but let's just get to it. We call the function by simply typing the name of the function. Oh and don't forget your parenthises when you call it. Read my warning below if your testing and you get the NameError:.

\**Watch Out\**

Now something to keep in mind when creating functions, is they need to be created before you can call them in your code. So you can't call your function at the top of your code then create it at the end. You will just get that big o'l NameError: name is not defined. The cool thing about Colab, which uses Jupyter, is you can run the cell with your defined function and then call your function anywhere.


```python
questionString()
```

    Why won't this print?
    Because I don't work till called



## How Function Parameters Work

Now in this objective we will show the basics and then come back at the end of the lesson with details. We don't want to melt your brains third objective in, we want you to first get the basics. We talked earlier about the parenthises and you can put parameters there to use in your function. Since functions are like a little block of program, you can make your own variables in there. These variables in your function are not effect by what's outside, but the variables you define outside can be used inside the funtion. Make sense or did we lose you, think I lost myself a little. I hope an example can explain it better. If not let me know if you need more clarification in the comments section below.


```python
var1 = "Outside Function"

print(var1)

def variableFunction():
  var2 = "Inside Function"
  print(var1, var2)

# print(var2)
# will not work, remove hashtag to try it out
variableFunction()
```

    Outside Function
    Outside Function Inside Function



```python
def useParameters(strValue):
  print(f"Here a {strValue}, There a {strValue}, Everywhere a {strValue}.")

useParameters("Pig")
useParameters("Cow")
useParameters("Chicken")
```

    Here a Pig, There a Pig, Everywhere a Pig.
    Here a Cow, There a Cow, Everywhere a Cow.
    Here a Chicken, There a Chicken, Everywhere a Chicken.


Does that remind you of MadLibs, where you fill in words, that ends up making a funny story. You could totally do that with a function! Functions can have as many parameters as you want. Just make sure that when you call your function that all the arguments have a value or you will get the big o'l TypeError: missing argument. Want to mention this too. A parameter is the variable listed inside the parentheses in the function definition. An argument is the value that is sent to the function when it is called. This can be interchangable between people, but to a function that is the correct use. We'll come back to this later...


## How To Use Return In Function

First what is a return value? Well since funtions are their own little thing. You don't always want to print the value that you create in the function. So Python has made it so you can get return values from a function with the keyword return. To accomplish this, at the end of your function you type return and the value that you want the function to return. This can be an answer to a math problem or some crazy formulation that iterates through a dictionary and returns all the keys. Can check more on [Dictionaries Here](https://www.journeyintopython.com/python-dictionaries/). Will keep it simple but you can build on to this example.


```python
def mathProblem(x):
  return 20 * x

mathProblem(20)
```




    400




```python
food = "turkey" # input("Pick A Food: ")
things = "stockings" # input("Pick a Thing(Plural): ")
action = "jump" # input("Pick an Action Verb: ")

def madLib(food, things, action):
  lib1 = print(f"There once was a gingerbread man who had two {things} for eyes and a {food} for a nose.\n"
    f"  He always said, '{action.capitalize()} as fast as you can, you can't catch me I'm the gingerbread man.'\n")
  lib2 = print(f"I saw this person eating a {food}.\n"
    f"  I {action}ed over there and asked, 'What {food} {things} do you like?'\n")
  return lib1, lib2

print(madLib(food, things, action))
```

    There once was a gingerbread man who had two stockings for eyes and a turkey for a nose.
      He always said, 'Jump as fast as you can, you can't catch me I'm the gingerbread man.'
    
    I saw this person eating a turkey.
      I jumped over there and asked, 'What turkey stockings do you like?'
    
    (None, None)


As you can see we typed out print() with our function name and then gave it the arguments that it needed, these are passed into the function parameters as variables. From there the function runs through the code and we ask for a return on the varibles we created in the function. We get a (None, None) because the madLib function is just performing an action not calculating values. One thing we didn't mention is that instead of using return you can use pass if your function is empty. This is handy if you are testing code and your function will be empty for the moment.


## What Parameters Can Functions Use

When creating functions you can use different kinds of parameters. The first being a regular variable and being able to have more than one.


```python
def oneParameter(x):
  return x

print(oneParameter(12))

def manyParameter(x,y,z):
  return x,y,z

print(manyParameter(10,2,34))
```

    12
    (10, 2, 34)


The second would be using an asterisk with the variable. This is used if you do not know how many parameters you will need.


```python
def asterisk(*x):
  return x

print(asterisk(1,2,3,4,5,6))
```

    (1, 2, 3, 4, 5, 6)


The next parameter would be using keywords to define the different arguments.


```python
def keyword(x,y,z):
  return x,y,z

print(keyword(y=10,z=36,x=15))
```

    (15, 10, 36)


You can then add a double asterisk along with keywords if you don't know how many arguments the keyword might have.


```python
def asteriskKeyword(**x):
  return x["xz"],x["xy"],x["xx"]

print(asteriskKeyword(xx=56,xy=10,xz=34))
```

    (34, 10, 56)


Another good parameter, that is also good coding practice, is giving your parameter a default value. Giving your parameter a default value makes it an optional argument. This works well because there is no need to specify a value for an optional argument when you call a function. This will keep you script from giving an error if an argument is not givin, because the default value will be used.


```python
def defaultKey(name="N\A",age="N\A"):
  x = f"Hello {name}, your age is {age}."
  return x

print(defaultKey())
print(defaultKey("Jack"))
print(defaultKey(age="23"))
print(defaultKey("Jane","36"))
```

    Hello None, your age is N\A.
    Hello Jack, your age is N\A.
    Hello None, your age is 23.
    Hello Jane, your age is 36.


You can use any data type for an argument in a functions parameter.


```python
fruitsList = ["orange", "plum", "mango"]

def foodFunction(food):
  for x in food:
    print(x)

foodFunction(fruitsList)
```

    orange
    plum
    mango



## What Is and How To Use Recursion

Recursion is, in it's basic form, where you call your function in your function. You should be very careful with recursion as it can be very easy for a function to never terminate, or one that uses excess amounts of memory or processor power. Though, when written carefully recursion can be very efficient. We will go into more details on recursion in another post.


```python
def recursion(x):
  if x > 0:
    print(recursion(x-1))
  else:
    x = 0
  return x

recursion(2)
```

    0
    1





    2




## Cheat Sheet Python Functions

### Syntax:

```
def function_name():
  code block
  return
```

### Parameters:

- `function_name(parameter):` When you require an argument
- `*args` Unknown Arguments (variable= x, y, z)
- `kwargs` Keyword Arguments (x= "x", y= "y")
- `**kwargs` Unknown Keyword Arguments (x= x[x, y, z])
- `argument = "defualt"` No Argument Uses Defualt Argument

### Global vs. Local variables:

Variables that are defined inside a function body are local variables, and those defined outside are global variables.

This means variables in your function are not effect by what's outside, but the variables you define outside can be used inside the function. When you call a function, the variables declared inside the function are local variables.

### Common Mistakes:

Calling a function before the function is defined

A function with parameters not getting the required arguments

A function does not execute code block till called

### Typical use:

To create reuseable code blocks that streamline coding. These functions are called user-defined functions. 
