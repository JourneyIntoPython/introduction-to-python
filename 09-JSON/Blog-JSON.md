
# Outline

Links:

[How to Use Google Colab for Python](https://www.journeyintopython.com/google-colab-python/)

[Python Variables and Data Types](https://www.journeyintopython.com/python-data-types-and-variables/)

[Python If Else Statement](https://www.journeyintopython.com/python-if-else/)

[Python While Loop](https://www.journeyintopython.com/python-while-loop/)

[Python for Loop](https://www.journeyintopython.com/python-for-loop/)

[Python Dictionaries and Nested Dictionary](https://www.journeyintopython.com/python-dictionaries/)


## Python JSON :

JSON is the go-to syntax for storing and exchanging data. Almost all programing languages support JSON to transfer imformation across the web. In Python you convert JSON to a Python dictionary. Here's a link if you want to know more on dictionaries [Python Dictionaries and Nested Dictionary](https://www.journeyintopython.com/python-dictionaries/). You have used JSON wether you knew it or not. A weather app can use a RESTful API to pull JSON from weather data centers to show your curent tempatures. Isn't that awesome?! The applications are incredible from dad jokes sent to your phone everyday, that's my favorite, to todays stock prices or maybe you want to keep track of Bitcoin. Now doesn't that make you want to learn how to put JSON to work for you? Your Journey Into Python begins now!

## What is JSON?

JavaScript Object Notation (JSON) is a text format used to represent structured data, like a Python dictionary, based on JavaScript object syntax `{[""]}`. It is commonly used for transmitting data in web applications to display the information that was collected.

## Course Objectives:

- JSON in Python
- Convert from JSON to Python
- Convert from Python to JSON
- Format the Result
- Order the Result
- Common Mistakes
- Cheat Sheet

## JSON in Python

JSON is built on two structures, a collection of name/value pairs and an ordered list of values. In most languages, this is realized as an array, vector, list, dictionary, or sequence. Though in Python the JSON is converted to a dictionary, which is now ordered since Python version 3.7.

This is how JSON is setup:

An ***object*** is an unordered set of name/value pairs `{}`.

An ***array*** is an ordered collection of values `[]`.

A ***value*** can be a *string* in double quotes, or a *number*, or true or false or null, or another object or an array `""`. These structures can be nested. A ***string*** is a sequence of blank or more characters, wrapped in double quotes. A ***number***, except that the octal and hexadecimal formats are not used. Here you can get a visual of how JSON can be setup [JSON.org](https://www.json.org/json-en.html). Here's an example of JSON, would be one single line but I indented so you can read it better.


```python
theObject = {
  "string":"string",
  "number":30,
  "true":True,
  "false":False,
  "null":None,
  "tuple":("tuple1","tuple2"),
  "list":["list1","list2"],
  "array":[
    {"string":"string","number":10},
    {"true":True,"nestedList":["list1","list2"]},
    {"ect...":"Can get as crazy as you need!!!"},
  ]
}
```

## Convert from JSON to Python

Python has a built-in package called json, which can be imported to work with JSON data. If you have a JSON string, you can parse it by using the json.loads() method. Take notice to the single qoutes on names varible, thats the difference between a JSON and Python dictionary.


```python
import json

names = '{"names":{"name1":"John","name2":"Jack","name3":"Jane"}}'

jsonPython = json.loads(names)

print(jsonPython["names"])
```

    {'name1': 'John', 'name2': 'Jack', 'name3': 'Jane'}


## Convert from Python to JSON

If you have a Python object, you can convert it into a JSON string by using the json.dumps() method.

When you convert from Python to JSON, Python objects are converted into the JSON (JavaScript) equivalent:

|Python| JSON |Python|JSON|
|------|------|------|----|
|dict  |Object|int   |Number|
|list  |Array |float |Number|
|tuple |Array |True  |true  |
|str   |String|False |false |
|None  |null  |


```python
import json

pythonDict = {
  "badJoke":
 {
    "question1":"Why did the slug call the police?",
    "answer1":"Because he was a'salted'."
 },
  "goodJoke":
 {
    "question2":"Why did the child get arrested for refusing to take a nap?",
    "answer":"Because she was resisting a'rest'."
 }
}

pythonJson = json.dumps(pythonDict, indent=4)

# When converted becomes one long string
print(pythonJson)
```

    {
        "badJoke": {
            "question1": "Why did the slug call the police?",
            "answer1": "Because he was a'salted'."
        },
        "goodJoke": {
            "question2": "Why did the child get arrested for refusing to take a nap?",
            "answer": "Because she was resisting a'rest'."
        }
    }


## Formatting the printed Result

JSON can be pretty ugly when printed with the standard python print function. The best method is to copy the JSON to an interface that can read it. For example, you could copy the output and paste it into a cell. This will help you with the structure because you can fold the JSON to help you identify what the JSON structure is. You could also do this in Visual Studio Code or almost any other IDE. 



```python
# Hover the mouse near the line number to see the code folding 

theObject = { # Notice you can fold here
  "string":"string",
  "number":30,
  "true":True,
  "false":False,
  "null":None,
  "tuple":("tuple1","tuple2"),
  "list":["list1","list2"],
  "array":[ # or you can fold here
    {"string":"string","number":10},
    {"true":True,"nestedList":["list1","list2"]},
    {"ect...":"Can get as crazy as you need!!!"},
  ]
}
```

However, if you are in a pinch, you can also use the pretty print function `pprint()`. It is easy to use, but has numerous features. You can also use `indent=` with the `json.dumps()` method. To read more about pprint check it out here: https://docs.python.org/3/library/pprint.html



```python
from pprint import pprint

pprint(theObject)

```

    {'array': [{'number': 10, 'string': 'string'},
               {'nestedList': ['list1', 'list2'], 'true': True},
               {'ect...': 'Can get as crazy as you need!!!'}],
     'false': False,
     'list': ['list1', 'list2'],
     'null': None,
     'number': 30,
     'string': 'string',
     'true': True,
     'tuple': ('tuple1', 'tuple2')}



```python
# Notice how much better pprint looks
print(theObject)
```

    {'string': 'string', 'number': 30, 'true': True, 'false': False, 'null': None, 'tuple': ('tuple1', 'tuple2'), 'list': ['list1', 'list2'], 'array': [{'string': 'string', 'number': 10}, {'true': True, 'nestedList': ['list1', 'list2']}, {'ect...': 'Can get as crazy as you need!!!'}]}


# Iterate Through Nested JSON Object to Get a Specific Value

Lets use the JSON object "theObject" and we want to pull out a few things. Lets start simple with the second list value "list2". Lets start by determine what keys are available using the keys function.


```python
theObject.keys()
```




    dict_keys(['string', 'number', 'true', 'false', 'null', 'tuple', 'list', 'array'])



Note here that list is one of the available keys. Lets pull that out.


```python
theObject['list']
```




    ['list1', 'list2']



Now we get our list. Lets pull out the second index.


```python
theObject['list'][1]
```




    'list2'



Now that we have an understanding we can try to grab values that are nested deeper in. We are going to try and grab the second index of the nestedList that is in the array.


```python
theObject['array']
```




    [{'number': 10, 'string': 'string'},
     {'nestedList': ['list1', 'list2'], 'true': True},
     {'ect...': 'Can get as crazy as you need!!!'}]



Now we see that the list contains the nestedList and it is the second index. 


```python
theObject['array'][1]
```




    {'nestedList': ['list1', 'list2'], 'true': True}



Now the data is a dictionary. We will have to pull out the nestedList as a key. 


```python
theObject['array'][1]['nestedList']
```




    ['list1', 'list2']



Ok, now we have the list. We want the second value in the list which is index 1.


```python
theObject['array'][1]['nestedList'][1]
```




    'list2'



Phew, we got it. 

JSON can at times be quite complex, so knowing your data types is very important. Remember when in doubt, you can use type() on the object to determine its data type. 

You might be thinking, does this happen in the real world?

It does. I was working on automating file analysis and was leveraging the VirusTotal API. Just look how complex this API object is: https://developers.virustotal.com/reference/files


## Order the Result

The `json.dumps()` method has parameters to order the keys in the result if you need them in alpabetical order. Use the `sort_keys` parameter to specify if the result should be sorted or not.


```python
json.dumps(theObject, sort_keys=True)
pprint(pythonDict)
```

    {'badJoke': {'answer1': "Because he was a'salted'.",
                 'question1': 'Why did the slug call the police?'},
     'goodJoke': {'answer': "Because she was resisting a'rest'.",
                  'question2': 'Why did the child get arrested for refusing to '
                               'take a nap?'}}


## Common Mistakes:

The biggest problem you will run into is syntax when converting your own python dict to json or json to python. Commas and colons in the wrong spot or missing will rise a `SyntaxError:`. Also keep in mind a JSON is just a really long string, so you can't get any values until it is converted.


## Cheat Sheet JSON

Printer friendly link to [Cheat Sheet](https://www.journeyintopython.com/cheat-json)

### JSON:

JSON is a syntax for storing and exchanging data, that uses text, written with JavaScript object notation.

### Syntax:

```
object = {
  "key":"value",
  "key":"value"
}
```

### Common mistakes:

Syntax: missing or wrong location of commas, colons, double qoutes.

Trying to find values before converting JSON to Python dictionary. Use the `type()` function if you're not sure what your varible is.

### Typical Use:

JSON is commonly used for transmitting data in web applications (e.g., sending data from the server to the client, so it can be displayed on a web page, or vice versa).

### Helpful functions:

```
import jsom
json.loads(json)
json.dumps(dict, indent=4)
pprint(json)
x.keys()

