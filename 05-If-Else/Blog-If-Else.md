
# Outline

Links:

[How to Use Google Colab for Python](https://www.journeyintopython.com/google-colab-python/)

[Python Variables and Data Types](https://www.journeyintopython.com/python-data-types-and-variables/)

[Python While Loop](https://www.journeyintopython.com/python-while-loop/)

[Learn the Python for Loop](https://www.journeyintopython.com/python-for-loop/)

[Dictionaries and Nested Dictionary in Python](https://www.journeyintopython.com/python-dictionaries/)


## Python if else :

Python if else statements is the bread and butter of scripting. The ability of a program to alter its code sequence is called branching, and it's a key component in making your scripts useful. Python if else statements allow you to create branching for your script, this instructs your computer to make decisions based on inputs. We as people make decisions everyday that change depending on our input, like if I'm broke try and make some money blogging. We will also go over the conditionals that are used with the Python if else statements. These python if else statements go hand-in-hand with the while and for loop. You can find those posts here. [*Python While Loop*](https://www.journeyintopython.com/python-while-loop/ "These can drive you crazy sometimes.") and [*Learn the Python for Loop*](https://www.journeyintopython.com/python-for-loop/ "Pretty cool how it prints each letter of a single string."). Here you can learn [*How to Use Google Colab for Python*](https://www.journeyintopython.com/google-colab-python/ "You totally want to know this!!! No need to download and IDE!") to play with the examples coming up.

## What is an if statement?

***If*** you knew that you wouldn't be reading this so...an if statement is used to evaluate a comparison using conditionals. We start with the if keyword, followed by our comparison, then with a colon. The body of the if statement is then indented to the right. If the comparison is True, the code in the body is executed. If the comparison evaluates to False, then the code block is skipped and will move on to the next line of code.

## Course Objectives:

[1]: ##python-if-statement-and-conditions
[2]: ##indentation
[3]: ##elif
[4]: ##else
[5]: ##short-hand-if-and-if-else
[6]: ##common-mistakes-new-developers-make:
[7]: ##python-cheat-sheet-to-help-code-faster:

- [Python If Statement and Conditions][1]
- [Indentation][2]
- [Elif][3]
- [Else][4]
- [Short Hand If and If Else][5]
- [Common Mistakes][6]
- [Cheat Sheet][7]


## Python If Statement and Conditions

Python uses the logic conditions from mathematics:

- Equal To: a == b
- Not Equal: a != b
- Less Than: a < b
- Less Than or Equal To: a <= b
- Greater Than: a > b
- Greater Than or Equal To: a >= b

These conditions can be used in several ways, most commonly in if statements and loops.

In this example my `wishbankaccount` is greater `>` than `mybankaccount` so it prints `"I am BROKE!"`


```python
mybankaccount = 0
wishbankaccount = 200
if wishbankaccount > mybankaccount:
  print("I am BROKE!!!")
```

    I am BROKE!!!



## Indentation

Python uses indentation (empty space at the beginning of a line) to define which body of code goes which line in the code.

Here I don't indent the body of the if statement so Python doesn't know it goes to the if statement above and gives a `IndentationError:`.


```python
mybankaccount = 0
wishbankaccount = 200
if wishbankaccount > mybankaccount:
print("I am BROKE!!!")
```


      File "<ipython-input-7-c66a01e0af15>", line 4
        print("I am BROKE!!!")
            ^
    IndentationError: expected an indented block




## Elif

The elif keyword is pythons way of saying, if the previous condition is not true, then try this condition.

So `wishbankaccount` is not `>` greater than `mybankaccount` which makes it False. Though `mybankaccount` is `==` equal to `wishbankaccount` so the condition is True and prints `"I broke even!"`.


```python
mybankaccount = 200
wishbankaccount = 200
if wishbankaccount > mybankaccount:
  print("I am BROKE!!!")
elif mybankaccount == wishbankaccount:
  print("I broke even!")
```

    I broke even!



## Else

The else keyword is for anything that isn't caught by the preceding conditions. Like if nothing else is True, do this instead.

The top two conditions are False so it prints `"I am not broke!"`.


```python
mybankaccount = 300
wishbankaccount = 200
if wishbankaccount > mybankaccount:
  print("I am BROKE!!!")
elif mybankaccount == wishbankaccount:
  print("I broke even!")
else:
  print("I am not broke!")
```

    I am not broke!



## Short Hand If and If Else

If you have only one statement to execute, you can put it on the same line as the if statement, or if you have only one if statement, and one else statement, you can put it all on the same line. Make sure with the if else short hand the the body goes first then the if condition, then else condition with body. Here indention is not needed, though this can get lengthy and hard for others to read your code.


```python
mybankaccount = "$2"
wishbankaccount = "$330"

if mybankaccount < wishbankaccount: print(f"mybankaccount = {mybankaccount}")

print(f"mybankaccount = {mybankaccount}") if mybankaccount > wishbankaccount else print(f"wishbankaccount = {wishbankaccount}")
```

    mybankaccount = $2
    wishbankaccount = $330



## Common Mistakes New Developers Make:

An `elif` statement must follow an `if` statement, and the body will only be executed if the `if` statement condition is False. The `if` body will be executed if condition1 is True. The `elif` body will be executed if condition1 is False and condition2 is True. The `else` body will be executed when all the other specified conditions are False.


## Python Cheat Sheet to Help Code Faster:

### Comparison operators

- Equal To: a == b
- Not Equal: a != b
- Less Than: a < b
- Less Than or Equal To: a <= b
- Greater Than: a > b
- Greater Than or Equal To: a >= b

### Logical operators
a and b: True if both a and b are True. False otherwise.

a or b: True if either a or b or both are True. False if both are False.

not a: True if a is False, False if a is True.

## Branching blocks

In Python, we branch our code using if, else and elif. This is the branching syntax:

```python
if condition1:
	body
elif condition2:
	body
else:
	body
```

Printer friendly link to [Cheat Sheet](https://www.journeyintopython.com/cheat-python-if-else/ "Thanks For reading to the end or just clicking this link")
