
# Outline

https://www.journeyintopython.com/google-colab-python/ "How to Use Google Colab for Python"

https://www.journeyintopython.com/python-data-types-and-variables/ "Python Variables and Data Types"

https://www.journeyintopython.com/python-while-loop/ "Python While Loop"

https://www.journeyintopython.com/python-for-loop/ "Python for Loop"

https://www.journeyintopython.com/python-dictionaries/ "Python Dictionaries and Nested Dictionary"

https://www.journeyintopython.com/string-formatting-operations/ "Python String Formatting Operations"

https://www.journeyintopython.com/operators/ "Python Operators"

https://www.journeyintopython.com/python-functions "Python Functions"

https://www.journeyintopython.com/python-classes "Python Classes"

https://www.journeyintopython.com/python-modules "Python Classes"


# Python Modules

In this course we are going to learn how to use Python Modules and how to create your own. Python Modules are a large part of programming by allowing developers to share their work with others. Not to mention that someone out there has ran into a promblem while coding and has come up with a solution. So when you are writing your own code you can import other peoples solutions. As well as incorparate into your own Python Modules for others to use. Python has a very large community of programmers that like to share their code. Altogether, in my opinion, Python is the most user friendly and most supportive community. Now for this course I will be using Colab for all my coding. My examples can be used in any IDE for Python. Alternatively I will have code snippets for those that want to use Colab. So lets begin you journey into Python Modules.

## What are Python Modules?

Python Modules can be refered to as a code library file. That file contains a group of classes and/or functions that can be used in another script.

If you would like to know more about functions and classes. You can find out at these courses. [Python Functions](https://www.journeyintopython.com/python-functions) and [Python Classes](https://www.journeyintopython.com/python-classes)

For example, you can have a module that handles the payment of your employees. Then have a module that keeps track of your current employees. You can then create a script that uses both modules to generate a report of each employees pay. This allows you to alter those modules without effecting the original Python Modules.

## Course Objectives:

- Create a Module and import it
- How to access functions and variables
- Learn different was to import Modules
- Getting info on built-in Modules
- A little on Packages
- Cheat Sheet

## Create Python Modules

Now that you understand what a module is and what it does. Let me give you an example below. As a rule your module will need to be saved as a .py file. This is because Python is looking through directories for that type of file format. In the case of my example below I will use Colab to download my code as a .py file. Although you can use any IDE for Python to save it as a .py file. Make sure that you save it in the directory that you are working in. Another thing you can do is use `import sys` and `print(sys.path)` to see what directories Python is looking in.

```Saved as Python File
# Save as your_module.py

print("Module imported successfully!")

villian = {
      "firstName":"Freddy",
      "lastName":"Krueger",
      "knownFor":"Terrorizing kids in their dreams."
      }

def print_function():
  print("You Horror Movie Villian is:")
  print(f"{firstName} {lastName}")
  print(f"Best Known For: {knownFor}")
  ```

Now by this point you should have a file save in you directory as a .py file. In the next two examples I show you how to set up Colab. This is so Colab can use your google drive to import your modules. I am saving my module in MyDrive so I can use Colab to import. The code below will mount my drive to Colab and then change the file path to MyDrive. You will want to run these cells if you are following along in Colab.


```python
# For Colab to get access to your google drive
from google.colab import drive
drive.mount("/content/drive",force_remount=True )
```

    Mounted at /content/drive



```python
# This will change the file path
import sys
sys.path.insert(0,"/content/drive/MyDrive")
```

Lets import our module. You can see that as soon as we import it prints something. This is because the module ran once as soon as it was imported, now keep that in mind. When the module is ran anything that is not in a function or class will excute. You have now created your first module and imported into your script. If you got an error message make sure you check the file path and the spelling. If your in Colab sometimes you have to restart runtime the run the cells again.


```python
import your_module
```

    Module imported successfully!


## Accessing Modules

If everything went correctly in the last example. We have imported our first module into our script. Now we want to access the variable that is inside the module. This is done by typing `your_module.villian`, this can also be followed by a key to give its value.


```python
print(your_module.villian)
print(your_module.villian["firstName"])
```

    {'firstName': 'Freddy', 'lastName': 'Krueger', 'knownFor': 'Terrorizing kids in their dreams.'}
    Freddy


Using modules helps clean up your code when you are working on complex projects. You are able to break up your code into smaller, more managable, peices of code. When you create modules the code should be functions or classes. To access the functions in a module is simalier to the variable. You type `your_module.print_function()` and the code inside the function will run.


```python
your_module.print_function()
```

    You Horror Movie Villian is:
    Freddy Krueger
    Best Known For: Terrorizing kids in their dreams.


## Different Imports

Alright, so we know how to create a module. We know have to import your module and access the contents. Though what if you just want to import some the content? Well Python lets you do that too in different ways. First I would like to go over the module import. When you import a module you can change the name to whatever you want. This can be done by importing the module as the name of your choice. Like the example below.


```python
import your_module as ym
```

Now I will go over how you can import certain content of you module. First you can type `from your_module import print_function` to import just that function. Keep in mind that you can not access that villian variable in the module now. Additionally you can import functions and call them what you want. Another way you can import functions is with the astirick after the import. This will allow access to all the content, but this is rarley used and frowned upon. The examples below I will show what each call will look like.


```python
from your_module import print_function
from your_module import print_function as pf
from your_module import*
```

## Built-in Modules

Anytime you use Python you have access to a wide range of modules. These modules can range from creating graphics to solving complex math formulas. You can find a list of built-in modules at [Pythons Standard Library](https://docs.python.org/3/library/) What I will talk about here is how to find out what functions they have and what they do. Once you have found a module that might help you with your script. You can use the `dir(module)` function to get a list of the useable functions in a module. Additionally once you found a function that you are looking at. You can use the `help(module.function)` function to get additional information on the function. Below are some examples of a module using the dir() and help function.


```python
print(dir(your_module))
print(help(your_module.villian))
```

    ['__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'print_function', 'villian']
    Help on dict object:
    
    class dict(object)
     |  dict() -> new empty dictionary
     |  dict(mapping) -> new dictionary initialized from a mapping object's
     |      (key, value) pairs
     |  dict(iterable) -> new dictionary initialized as if via:
     |      d = {}
     |      for k, v in iterable:
     |          d[k] = v
     |  dict(**kwargs) -> new dictionary initialized with the name=value pairs
     |      in the keyword argument list.  For example:  dict(one=1, two=2)
     |  
     |  Methods defined here:
     |  
     |  __contains__(self, key, /)
     |      True if the dictionary has the specified key, else False.
     |  
     |  __delitem__(self, key, /)
     |      Delete self[key].
     |  
     |  __eq__(self, value, /)
     |      Return self==value.
     |  
     |  __ge__(self, value, /)
     |      Return self>=value.
     |  
     |  __getattribute__(self, name, /)
     |      Return getattr(self, name).
     |  
     |  __getitem__(...)
     |      x.__getitem__(y) <==> x[y]
     |  
     |  __gt__(self, value, /)
     |      Return self>value.
     |  
     |  __init__(self, /, *args, **kwargs)
     |      Initialize self.  See help(type(self)) for accurate signature.
     |  
     |  __iter__(self, /)
     |      Implement iter(self).
     |  
     |  __le__(self, value, /)
     |      Return self<=value.
     |  
     |  __len__(self, /)
     |      Return len(self).
     |  
     |  __lt__(self, value, /)
     |      Return self<value.
     |  
     |  __ne__(self, value, /)
     |      Return self!=value.
     |  
     |  __repr__(self, /)
     |      Return repr(self).
     |  
     |  __setitem__(self, key, value, /)
     |      Set self[key] to value.
     |  
     |  __sizeof__(...)
     |      D.__sizeof__() -> size of D in memory, in bytes
     |  
     |  clear(...)
     |      D.clear() -> None.  Remove all items from D.
     |  
     |  copy(...)
     |      D.copy() -> a shallow copy of D
     |  
     |  get(self, key, default=None, /)
     |      Return the value for key if key is in the dictionary, else default.
     |  
     |  items(...)
     |      D.items() -> a set-like object providing a view on D's items
     |  
     |  keys(...)
     |      D.keys() -> a set-like object providing a view on D's keys
     |  
     |  pop(...)
     |      D.pop(k[,d]) -> v, remove specified key and return the corresponding value.
     |      If key is not found, d is returned if given, otherwise KeyError is raised
     |  
     |  popitem(...)
     |      D.popitem() -> (k, v), remove and return some (key, value) pair as a
     |      2-tuple; but raise KeyError if D is empty.
     |  
     |  setdefault(self, key, default=None, /)
     |      Insert key with a value of default if key is not in the dictionary.
     |      
     |      Return the value for key if key is in the dictionary, else default.
     |  
     |  update(...)
     |      D.update([E, ]**F) -> None.  Update D from dict/iterable E and F.
     |      If E is present and has a .keys() method, then does:  for k in E: D[k] = E[k]
     |      If E is present and lacks a .keys() method, then does:  for k, v in E: D[k] = v
     |      In either case, this is followed by: for k in F:  D[k] = F[k]
     |  
     |  values(...)
     |      D.values() -> an object providing a view on D's values
     |  
     |  ----------------------------------------------------------------------
     |  Class methods defined here:
     |  
     |  fromkeys(iterable, value=None, /) from builtins.type
     |      Create a new dictionary with keys from iterable and values set to value.
     |  
     |  ----------------------------------------------------------------------
     |  Static methods defined here:
     |  
     |  __new__(*args, **kwargs) from builtins.type
     |      Create and return a new object.  See help(type) for accurate signature.
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes defined here:
     |  
     |  __hash__ = None
    
    None


## An Overview of Packages

Packages are a namespace which is comprised of other packages and modules They're just like directories on your computer with some differences. Every package in Python is a directory which **must** include a unique file referred to as an \_\_init__.py. This file can be empty and it shows Python that this is a directory of a Python package. This means that it can be imported just like any other module. Below is a minimum of what a Python package must include.


```python
main_directory_folder
  __init__.py
    # This file is required
    # File can be empty
  your_module.py
    # Has functions to use

import main_directory_folder.your_module
from main_directory_folder import your_module

```

## Cheat Sheet Python Modules:

Python Modules can be refered to as a code library file. That file contains a group of classes and/or functions that can be used in another script.

### Tips

- Module has to be saved as a .py file
- Python needs to know where to look for your module
- Use sys.path to find where Python is looking for module
- Use dir(module) and help(module.function) for module information

### Functions

- import sys - Python's system management module
- sys.path - Displays the paths that Python looks for modules
- import module - How to import modules
- import module as ym - Rename the module
- from module import function - Import just one function of module
- from module import function as pf - Rename the function
- from module import* - Used to import all functions of module

### Common Mistakes

Incorrect PYTHONPATH for module

Forgetting you period between module name and function name

### Typical Use

Modules are best used to seperate complex script into managable groups, with  similiar functions. This will also clean up your code and be more reader friendly.
